var ReactDOM = require('react-dom');
var Footer = require('./component/footer.jsx');
var Navigator = require('./component/navigator.jsx');

ReactDOM.render(<div>
    <Navigator/>
    <Footer/>
</div>, document.body);